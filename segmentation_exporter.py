# -*- coding: utf-8 -*-
import os
import sys
import argparse
import glob
import json
import numpy as np
import tqdm
from PIL import Image

import matplotlib.pyplot as plt
from matplotlib import cm

try:
    env_path = os.path.dirname(__file__)
    if env_path not in sys.path:
        sys.path.append(env_path)
    from cryptomatte_renderer import CryptoRenderer
except ImportError:
    print('CryptoRenderer import failed. Maybe not launching from Blender? Try calling: blender --python segmentation_exporter.py -- --help')


def parse_arguments():
    parser = argparse.ArgumentParser(description='',
				     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--dataset', help='path to dataset', required=True)
    parser.add_argument('--seq', help='sequence', required=True)
    parser.add_argument('--gen_demo', help='generate demo files', action='store_true')
    
    # get the arguments after -- (which blender ignores)
    # https://blender.stackexchange.com/a/8405
    argv = sys.argv
    try:
        index = argv.index("--") + 1
    except ValueError:
        index = len(argv)

    argv = argv[index:]
    return parser.parse_args(argv)

def run(args):
    if args.seq == 'all':
        sequences = _get_sequence_list(args.dataset)
    else:
        sequences = [args.seq]

    if args.gen_demo:
        import shutil
        for i in range(1, 20):
            frame_name = '{:08d}'.format(i)
            mask = load_unity_instance_mask(args.dataset, args.seq, frame_name)
            out_mask = Image.fromarray((mask*255).astype(np.uint8))
            out_path = os.path.join('demo_sequence', 'instance_mask_{}.png'.format(frame_name))
            out_mask.save(out_path)

            base_sequence, object_id = args.seq.split('_')
            img = os.path.join(args.dataset, 'images', 'octane_full_exp', base_sequence, '{}.png'.format(frame_name))
            img = Image.open(img)
            img.load()
            out_img = os.path.join('demo_sequence', 'frame_{}.jpg'.format(frame_name))
            img.save(out_img)

            matte = os.path.join(args.dataset, 'annotations', 'cryptomatte', 'octane_full_exp', base_sequence, '{}.exr'.format(frame_name))
            out_matte = os.path.join('demo_sequence', 'cryptomatte_{}.exr'.format(frame_name))
            shutil.copyfile(matte, out_matte)

        sys.exit(1)

    for sequence in tqdm.tqdm(sequences, desc="Sequence"):
        export_masks(args.dataset, sequence)

def export_masks(dataset, sequence):
    base_sequence, object_id = sequence.split('_')
    rgb_paths = sorted(glob.glob(os.path.join(dataset, 'images', 'octane_full_exp', base_sequence,
                                              '*.png')))
    matte_paths = sorted(glob.glob(os.path.join(dataset, 'annotations', 'cryptomatte',
                                                'octane_full_exp', base_sequence,
                                                '*.exr')))
    frame_name = os.path.splitext(os.path.basename(rgb_paths[0]))[0]
    unity_instance_mask = load_unity_instance_mask(dataset, sequence, frame_name)
    assert len(rgb_paths) == len(matte_paths)
    renderer = CryptoRenderer()
    renderer.set_inputs(rgb_paths[0], matte_paths[0])

    object_name, iou = renderer.find_name_by_mask(unity_instance_mask)
    # print('object_name: {}'.format(object_name))
    # print('iou: {}'.format(iou))
    # plt.subplot(1, 2, 1)
    # plt.imshow(all_objects[object_name])
    # plt.subplot(1, 2, 2)
    # plt.imshow(unity_instance_mask)
    # plt.show()
    # sys.exit(1)
    # object_name = "Mesh_0000000b"

    export_dir = f'/home/jonas/dev/phd/data/kryptomat/cryptomatte_out/{sequence}/'
    if not os.path.exists(export_dir):
        os.makedirs(export_dir)

    for rgb_path, matte_path in tqdm.tqdm(list(zip(rgb_paths, matte_paths))):
        frame_name = os.path.splitext(os.path.basename(rgb_path))[0]
        renderer.set_inputs(rgb_path, matte_path)
        renderer.pick_object(object_name)
        mask = renderer.get_image(mask=True)
        # out_mask = Image.fromarray((mask*255).astype(np.uint8))
        out_mask = Image.fromarray((cm.viridis(mask)*255).astype(np.uint8))
        out_path = os.path.join(export_dir, f'{frame_name}.png')
        out_mask.save(out_path)
    return 0

def load_unity_instance_mask(dataset, seq, frame_name):
    base_sequence_name, obj_id = seq.split('_')
    seg_path = os.path.join(dataset, 'annotations', 'instance',
                            base_sequence_name, f'{frame_name}.png')
    meta_path = os.path.join(dataset, 'annotations', 'meta', base_sequence_name,
                             f'{seq}.json')
    with open(meta_path, 'r') as fin:
        meta_dict = json.loads(fin.read())
    instance = Image.open(seg_path)
    instance.load()
    instance = np.asarray(instance, dtype=np.uint8)
    b_ok = instance[..., 0] == meta_dict['instance_color'][0]
    g_ok = instance[..., 1] == meta_dict['instance_color'][1]
    r_ok = instance[..., 2] == meta_dict['instance_color'][2]

    mask = np.logical_and.reduce((r_ok, g_ok, b_ok))
    return np.float32(mask)

def _get_sequence_list(base_path):
    bbox_anno_dir = os.path.join(base_path, 'annotations', 'amodal_bbox')
    orig_sequences = [x for x in os.listdir(bbox_anno_dir)
                        if os.path.isdir(os.path.join(bbox_anno_dir, x))]

    allowed_sequences = None  # all

    sequences = []
    for orig_seq in orig_sequences:
        seq_bbox_anno_dir = os.path.join(bbox_anno_dir, orig_seq)
        subsequences = []

        for subseq_name in os.listdir(seq_bbox_anno_dir):
            if os.path.isfile(os.path.join(seq_bbox_anno_dir, subseq_name)):
                if allowed_sequences is not None:
                    if subseq_name not in allowed_sequences:
                        continue
                subsequences.append(subseq_name)

        sequences += subsequences

    sequences.sort()
    return sequences

def main():
    args = parse_arguments()
    return run(args)

if __name__ == '__main__':
    main()
