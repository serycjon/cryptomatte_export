import numpy as np
try:
    import bpy
except ImportError:
    print('bpy import failed. Maybe not launching from Blender? Try calling: blender --python cryptomatte_renderer.py')
    import sys
    sys.exit(1)
import struct
import OpenEXR
import json

def _get_cryptomatte_exr_objects(path):
    ''' Load cryptomatte object names and hashes from cryptomatte exr file metadata '''
    exr = OpenEXR.InputFile(path)
    header = exr.header()
    mesh_dict_string = None
    for k, v in header.items():
        if 'manifest' in k and 'cryptomatte' in k:
            assert mesh_dict_string is None
            mesh_dict_string = v

    mesh_dict = json.loads(mesh_dict_string)
    return mesh_dict

def _hash_to_float(hash_str):
    ''' Convert hash string to cryptomatte float id '''
    hash_int = int(hash_str, 16)
    packed = struct.pack('<L', hash_int & 0xffffffff)  # convert to bytes
    hash_float = struct.unpack('<f', packed)[0] # interpret as float
    return hash_float

class CryptoRenderer():
    def __init__(self):
        ''' Set up the Blender compositor nodes '''
        # switch on nodes and get reference
        bpy.context.scene.use_nodes = True
        tree = bpy.context.scene.node_tree
        links = tree.links
        self.links = links

        # clear default nodes
        for node in tree.nodes:
            tree.nodes.remove(node)

        # create input image node
        image_node = tree.nodes.new(type='CompositorNodeImage')
        image_node.location = 0,0
        # image_node.image = img
        self.image_node = image_node
        self.img = None

        # create input kryptomat node
        mat_node = tree.nodes.new('CompositorNodeImage')
        mat_node.location = 0,500
        # mat_node.image = mat_img
        self.mat_node = mat_node
        self.mat_img = None

        # kryptomat node
        kryptomat_node = tree.nodes.new('CompositorNodeCryptomatte')
        kryptomat_node.location = 400,500
        # kryptomat_node.matte_id = "<{}>".format(crypto_float)
        self.kryptomat_node = kryptomat_node

        # https://blender.stackexchange.com/a/102838
        out_switch_node = tree.nodes.new('CompositorNodeSwitch')
        out_switch_node.location = 600,500
        self.out_switch_node = out_switch_node

        out_node = tree.nodes.new('CompositorNodeViewer')
        out_node.location = 800,500

        link = links.new(image_node.outputs[0], kryptomat_node.inputs[0])
        link = links.new(kryptomat_node.outputs[1], out_switch_node.inputs[0])
        link = links.new(kryptomat_node.outputs[0], out_switch_node.inputs[1])
        link = links.new(out_switch_node.outputs[0], out_node.inputs[0])

        self.linked = False  # cryptomatte links setup after loading the images

    def _link_nodes(self):
        ''' Set the cryptomatte links - only possible after the cryptomatte image has been loaded. '''
        if self.linked:
            return
        link = self.links.new(self.mat_node.outputs[0], self.kryptomat_node.inputs[1])
        link = self.links.new(self.mat_node.outputs[1], self.kryptomat_node.inputs[2])
        link = self.links.new(self.mat_node.outputs[2], self.kryptomat_node.inputs[3])
        self.linked = True

    def set_inputs(self, img_path, matte_path):
        ''' Load the images and set them to the compositor input nodes '''
        self.img_path = img_path
        self.matte_path = matte_path

        if self.img is not None:
            self.img.buffers_free()
            self.mat_img.buffers_free()

        self.img = bpy.data.images.load(img_path)
        self.mat_img = bpy.data.images.load(matte_path)

        self.image_node.image = self.img
        self.mat_node.image = self.mat_img

        self._link_nodes()

        self.objects = _get_cryptomatte_exr_objects(matte_path)

    def get_name_by_float(self, float_hash):
        ''' Get mesh name by float hash (e.g. hand-picked in blender) '''
        best_object_name = None
        best_dist = float('inf')
        for object_name, object_hash in self.objects.items():
            object_float_hash = _hash_to_float(object_hash)
            dist = abs(float_hash - object_float_hash)
            if dist < best_dist:
                best_object_name = object_name
                best_dist = dist
        return best_object_name

    def pick_object(self, name): 
        ''' Select cryptomatte object '''
        object_hash = self.objects[name]
        float_hash = _hash_to_float(object_hash)
        self.kryptomat_node.matte_id = "<{}>".format(float_hash)

    def get_image(self, mask=True):
        ''' render alpha mask or masked RGBA image into a numpy array '''
        if mask:
            self.out_switch_node.check = False
        else:
            self.out_switch_node.check = True

        bpy.ops.render.render()
        out_img = bpy.data.images['Viewer Node']
        size = out_img.size
        W, H = size
        channels = out_img.channels
        # https://ammous88.wordpress.com/2015/01/16/blender-access-render-results-pixels-directly-from-python-2/
        pixels = bpy.data.images['Viewer Node'].pixels
        np_pixels = np.array(pixels).reshape(H, W, channels)[::-1, ...]

        if mask:
            np_pixels = np_pixels[:, :, 0]  # all R, G, B channels should be equal
        return np_pixels.copy()

    def get_named_masks(self):
        ''' Returns dictionary with mesh name as key, corresponding matte as value '''
        masks = {}
        for obj_name in self.objects.keys():
            self.pick_object(obj_name)
            mask = self.get_image(mask=True)
            masks[obj_name] = mask

        return masks

    def find_name_by_mask(self, query_mask):
        ''' Finds mesh name best matching the query mask (by IoU score) '''
        assert len(query_mask.shape) == 2
        all_masks_dict = self.get_named_masks()
        hard_query = query_mask > 0

        best_iou = 0
        best_name = None
        for object_name, object_matte in all_masks_dict.items():
            hard_matte = object_matte > 0

            iou = _compute_mask_iou(hard_matte, hard_query)

            if iou > best_iou:
                best_iou = iou
                best_name = object_name

        return best_name, best_iou
    
def _compute_mask_iou(mask_a, mask_b):
    ''' Compute intersection-over-union score for two (HxW) masks '''
    if mask_a.shape != mask_b.shape:
        raise Exception("Mask shapes not the same! ({mask_a.shape} vs {mask_b.shape}) Maybe you are running Blender in background mode?")
    both = np.stack((mask_a, mask_b), axis=0)
    intersection_mask = np.all(both, axis=0)
    union_mask = np.any(both, axis=0)

    intersection = np.sum(intersection_mask)
    union = np.sum(union_mask)

    if union > 0:
        iou = intersection / float(union)
    else:
        iou = 0
    return iou

def demo_simple():
    import matplotlib.pyplot as plt
    renderer = CryptoRenderer()
    img_path = "demo_frame.png"
    mat_path = "demo_cryptomatte.exr"

    renderer.set_inputs(img_path, mat_path)
    # copy selected float id from Blender cryptomatte node
    # <35.262821197509766>
    # mesh_name = renderer.get_name_by_float(35.262821197509766)  # = Mesh_0000000b
    mesh_name = 'Mesh_0000000b'
    renderer.pick_object(mesh_name)
    mask = renderer.get_image(mask=True)
    rgba = renderer.get_image(mask=False)

    plt.subplot(1, 2, 1)
    plt.imshow(rgba)
    plt.subplot(1, 2, 2)
    plt.imshow(mask)
    plt.show()

def demo_from_instance_mask():
    import os
    from PIL import Image
    renderer = CryptoRenderer()
    img_path = 'demo_sequence/frame_00000001.jpg'
    matte_path = 'demo_sequence/cryptomatte_00000001.exr'
    renderer.set_inputs(img_path, matte_path)

    # load hard segmentation mask from unity
    mask_path = 'demo_sequence/instance_mask_00000001.png'
    unity_instance_mask = Image.open(mask_path)
    unity_instance_mask.load()
    unity_instance_mask = np.asarray(unity_instance_mask, dtype=np.uint8)

    # find corresponding cryptomatte object name
    print('Looking up cryptomatte object name, this will take some time...')
    object_name, iou = renderer.find_name_by_mask(unity_instance_mask)

    print('Done. Rendering masks...')
    for i in range(1, 20):
        frame_name = '{:08d}'.format(i)

        img_path = os.path.join('demo_sequence', 'frame_{}.jpg'.format(frame_name))
        matte_path = os.path.join('demo_sequence', 'cryptomatte_{}.exr'.format(frame_name))
        renderer.set_inputs(img_path, matte_path)
        renderer.pick_object(object_name)

        mask = renderer.get_image(mask=True)
        out_mask = Image.fromarray((mask*255).astype(np.uint8))
        # from matplotlib import cm
        # out_mask = Image.fromarray((cm.viridis(mask)*255).astype(np.uint8))  # colormapped mask
        out_path = os.path.join('demo_sequence', 'output_matte_{}.png'.format(frame_name))
        out_mask.save(out_path)
        print(f'Saved matte to {out_path}')

if __name__ == '__main__':
    # demo_simple()
    demo_from_instance_mask()
